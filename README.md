Tryton Replication
==================

Database replication capabilities between Tryton servers.

[![Build Status](https://drone.io/gitlab.com/datalifeit/tryton_replication/status.png)](https://drone.io/gitlab.com/datalifeit/tryton_replication/latest)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
