#!/usr/bin/env python
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from setuptools import setup
import os
import io


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


setup(name='tryton_replication',
    version='6.0.3',
    description='Tryton Replication',
    long_description=read('README.md'),
    author='Datalife S.Coop.',
    author_email='info@datalifeit.es',
    url='https://gitlab.com/datalifeit/tryton_replication',
    license='GPL-3',
    packages=['tryton_replication'],
    package_data={
        'tryton_replication': ['*.py']
    },
    install_requires=[
        'trytond>=6.0'],
    zip_safe=False,
    )
