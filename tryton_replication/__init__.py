# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import collections
import logging
import sys
import traceback
import uuid
from datetime import datetime
from itertools import groupby

from dateutil.relativedelta import relativedelta

from trytond import backend
from trytond.exceptions import UserError
from trytond.model import ModelSQL, ModelView, Unique, Workflow, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, If, Not
from trytond.rpc import RPC
from trytond.transaction import Transaction

__all__ = ['ReplManager', 'ReplDestMixin', 'ReplSrcMixin',
    'ReplLocalIdDestMixin', 'ReplLocalIdSrcMixin', 'ReplUUIDSrcMixin',
    'ReplUUIdMasterDuplexMixin', 'ReplUUIdSlavDuplexMixin', 'ReplDeleted',
    'Warning_', 'User']

BATCH_LEN = 1000
current_action = ('action', 'model_name')
logger = logging.getLogger(__name__)


class ReplicateError(Exception):
    pass


class InexistantTarget(ReplicateError):

    def __init__(self, target, id_):
        self.target = target
        self.id_ = id_

    def __str__(self):
        return 'Target "%s" has not record identified by "%s"' % (
            self.target.__name__, self.id_)


class UnspecifiedUniqueIdColum(ReplicateError):
    def __init__(self, model_name):
        self.model_name = model_name

    def __str__(self):
        return 'Unspecified Unique Id Column for Model "%s"' % self.model_name


class FSDataNotLinked(ReplicateError):
    def __init__(self, model, fs_id):
        self.model = model
        self.fs_id = fs_id

    def __str__(self):
        return ('File system data not linked for Model "%s", "%s"' %
            (self.model.__name__, self.fs_id))


class SourceReplicateError(ReplicateError):
    def __init__(self, error_description):
        self.error_description = error_description

    def __str__(self):
        return self.error_description


class ReplManager(object):

    _dest_models_list = None
    _src_models_list = None

    @classmethod
    def _get_replicated_models(cls, ModelType):
        pool = Pool()
        models, names = [], []

        def explore_model(Model):
            names.append(Model.__name__)
            deferred_models = []
            for f_name, field in list(Model._fields.items()):
                if field._type == 'many2one':
                    target = field.get_target()
                    model_name = target.__name__
                    if (issubclass(target, ModelType) and
                            model_name not in names):
                        if f_name in Model._sync_deferred_fields:
                            deferred_models.append(target)
                        else:
                            explore_model(target)
            if Model.static_depends:
                for model_name in Model.static_depends:
                    target = pool.get(model_name)
                    if (issubclass(target, ModelType) and
                            model_name not in names):
                        explore_model(target)
            models.append(Model)
            if deferred_models:
                models.extend(deferred_models)

        def get_models_by_priority():
            priority_models = {}
            for name, Model in pool.iterobject():
                priority_value = getattr(Model, '_sync_priority', None)
                if priority_value:
                    priority_models.setdefault(priority_value, [])
                    priority_models[priority_value].append(Model)
            return collections.OrderedDict(sorted(priority_models.items()))

        priority_models = get_models_by_priority()
        for Models in list(priority_models.values()):
            for Model in Models:
                if (issubclass(Model, ModelType) and Model.__name__
                        not in names):
                    explore_model(Model)
        return models

    @classmethod
    def replicate(cls, proxy_, directions=('pull', 'push'), models=None):
        assert directions
        pull_info, push_info = {}, {}
        if 'pull' in directions:
            pull_info = cls.pull_all(proxy_, models=models)
        if 'push' in directions:
            push_info = cls.push_all(proxy_, models=models)
        return {'push': push_info, 'pull': pull_info}

    @classmethod
    def pull_all(cls, proxy_, models=None):
        global current_action
        logger.info('Pulling models ...')
        pool = Pool()
        if models is None:
            if cls._dest_models_list is None:
                cls._dest_models_list = cls._get_replicated_models(
                    ReplDestMixin)
            models_list = cls._dest_models_list[:]
        else:
            models_list = [pool.get(model) for model in models]
        info = {}
        models_list.reverse()
        for m in models_list:
            m.reset_session()
        with Transaction().set_context(_check_access=False, active_test=False):
            while models_list:
                Model = models_list.pop()
                logger.info('Pulling Model %s ' % Model.__name__)
                current_action = ('pull', Model.__name__)
                pulled = Model.pull_records(proxy_)
                for model_name in Model.to_process_again():
                    to_pull_model = pool.get(model_name)
                    if to_pull_model in models_list:
                        models_list.remove(to_pull_model)
                    models_list.append(to_pull_model)
                Model._to_process_again = []
                if pulled > 0:
                    info.setdefault(Model.__name__, 0)
                    info[Model.__name__] += pulled

        return info

    @classmethod
    def init_db(cls, rpc_session):
        cls._dest_models_list = None
        cls._src_models_list = None
        pool = Pool()
        for name, Model in pool.iterobject():
            if (issubclass(Model, ReplDestMixin)
                    and Model.__name__ != 'res.user'):
                try:
                    Model.adjust_fs_data(rpc_session)
                except Exception as exception:
                    raise SourceReplicateError(exception.args[0])

        # map remote user and local user
        User = Pool().get('res.user')
        User.write([User(Transaction().user)],
            {User.unique_id_column: rpc_session._user})

    @classmethod
    def push_all(cls, proxy_, models=None):
        global current_action
        logger.info('Pushing models ...')
        pool = Pool()
        if models is None:
            if cls._src_models_list is None:
                cls._src_models_list = cls._get_replicated_models(ReplSrcMixin)
            models_list = cls._src_models_list[:]
        else:
            models_list = [pool.get(model) for model in models]

        info = {}
        models_list.reverse()
        for m in models_list:
            m.rpc_reset_session(proxy_)
        with Transaction().set_context(active_test=False):
            while models_list:
                Model = models_list.pop()
                logger.info('Pushing Model %s ' % Model.__name__)
                current_action = ('push', Model.__name__)
                try:
                    rec_count = Model.push_records(proxy_,
                        update_modified=bool(not models))
                except Exception as e:
                    info[Model.__name__] = e
                    traceback.print_exc(file=sys.stdout)
                    break
                for model_name in Model.rpc_to_process_again(proxy_):
                    to_process_model = pool.get(model_name)
                    if not isinstance(to_process_model, ReplSrcMixin):
                        continue
                    if to_process_model in models_list:
                        models_list.remove(to_process_model)
                    models_list.append(to_process_model)
                if rec_count > 0:
                    info.setdefault(Model.__name__, 0)
                    info[Model.__name__] += rec_count

        return info


class ReplMixin(object):
    """Base class for replicate models"""

    unique_id_column = None
    static_depends = None
    # determines the name of remote model. if empty use cls.__name__
    _remote_model = None
    _fs_data = None
    _pull_context = {'synch': True}
    _required_ids = None
    # determines if must be notified of synch changes
    _notify = False
    # determines the field filled with a ir.sequence value
    _sequenced_field = ''
    # determines the priority of the model along synch
    _sync_priority = 10
    # determines if must throw the UserError coming from remote source
    _source_replicate_error = False

    @classmethod
    def __setup__(cls):
        super(ReplMixin, cls).__setup__()
        if not cls.unique_id_column:
            raise UnspecifiedUniqueIdColum(cls.__name__)

    @classmethod
    def _check_fs_data(cls):
        if cls._fs_data is not None:
            return None
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        fs_info_recs = ModelData.search([('model', '=', cls.__name__)])
        records = cls.search([
            ('id', 'in', [r.db_id for r in fs_info_recs]),
            (cls.unique_id_column, '=', None)])
        if records:
            fs_info = {r.db_id: r.fs_id for r in fs_info_recs}
        return [(fs_info[r.id], r.id) for r in records]

    @classmethod
    def _update_fs_data(cls, local_ids, remote_ids):
        recs = {r.id: r for r in
           cls.browse([fs_id[1] for fs_id in local_ids])}
        for fs_id in local_ids:
            remote_id = remote_ids.get(fs_id[0], None)
            if not remote_id:
                raise FSDataNotLinked(cls, fs_id[0])
            cls.write([recs[fs_id[1]]], {
                cls.unique_id_column: remote_id[0]})
        cls._fs_data = True

    @classmethod
    def get_fs_info(cls, fs_ids):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        data_recs = ModelData.search([('fs_id', 'in', fs_ids)])
        db_ids = [r.db_id for r in data_recs]
        fs_info = {r.db_id: r.fs_id for r in data_recs}
        with Transaction().set_user(0):
            # skip rules
            records = cls.browse(db_ids)
        return {fs_info[r.id]: (getattr(r, cls.unique_id_column),
            r.modify_date, r.modify_date.microsecond)
            for r in records}

    @classmethod
    def adjust_fs_data(cls, rpc_session):
        local_fs_info = cls._check_fs_data()
        if local_fs_info:
            rpc_model_name = cls._get_remote_name()
            fs_ids = [fs_id[0] for fs_id in local_fs_info]
            remote_fs_info = rpc_session.execute('model',
                rpc_model_name,
                'get_fs_info', fs_ids)
            if remote_fs_info:
                cls._update_fs_data(local_fs_info, remote_fs_info)

    @classmethod
    def _get_remote_name(cls, object_type='model'):
        assert object_type in ('model', 'wizard')
        return cls._remote_model or cls.__name__

    @classmethod
    def set_required_ids(cls, unique_id):
        if cls._required_ids is None:
            cls._required_ids = []
        if unique_id not in cls._required_ids:
            cls._required_ids.append(unique_id)

    @classmethod
    def _update_date_model(cls, date_attribute, date_value):
        Model = Pool().get('ir.model')

        if not date_value:
            return

        model, = Model.search(['model', '=', cls.__name__])
        setattr(model, date_attribute, date_value)
        with Transaction().set_context(_check_access=False):
            model.save()
            Transaction().commit()


class ReplDestMixin(ReplMixin):
    """Base class for destination replicate models"""

    _session_records = []
    _to_process_again = []
    """
    _sync_deferred_fields:
        List of fields that will be deferred on 'create'
        changes. The values will be saved after record creation at the end of
        replication.
    """
    _sync_deferred_fields = []
    """
    _sync_field_mapping:
        Dict with remote field name as key and local field name as value.
        Only for fields with different name between source and destination
    """
    _sync_field_mapping = {}

    @classmethod
    def __setup__(cls):
        super(ReplDestMixin, cls).__setup__()
        cls.__rpc__.update({
            'reset_session': RPC(readonly=False),
            'to_process_again': RPC(readonly=False),
            'get_repl_data': RPC(readonly=False),
            'synchronise': RPC(readonly=False)})

        for attr in dir(cls):
            field = getattr(cls, attr, None)
            if not isinstance(field, fields.Field):
                continue
            if not field:
                continue
            if isinstance(field, fields.Function):
                field = field._field
            # disable domain and required on synch
            if field.domain:
                field.domain = [If(
                    Bool(Eval('context', {}).get('synch', False)),
                    [], field.domain)]
            if 'required' in field.states:
                field.states['required'] &= Not(
                    Bool(Eval('context', {}).get('synch', False)))

    @classmethod
    def validate_on_sync(cls):
        return False

    @classmethod
    def validate(cls, records):
        if (not Transaction().context.get('synch', False)
                or cls.validate_on_sync()):
            super().validate(records)

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    @classmethod
    def _get_wire_ids(cls):
        with Transaction().set_context(active_test=False):
            return [getattr(r, cls.unique_id_column) for r in cls.search([])]

    @classmethod
    def get_repl_data(cls):
        result = {}
        field_names = []
        for f_name, field in list(cls._fields.items()):
            if f_name not in ('id', 'create_uid', 'write_uid', 'create_date',
                    'write_date', 'rec_name', 'synch_date'):
                field_names.append(f_name)
        if field_names:
            result['field_names'] = field_names

        if cls._required_ids:
            result['required_ids'] = list(set(cls._required_ids))
            cls._required_ids = []

        result['l_create'], result['l_update'], result['l_delete'] = (cls.
            _search_model_dates())
        return result

    @classmethod
    def _search_model_dates(cls):
        Model = Pool().get('ir.model')

        model, = Model.search(['model', '=', cls.__name__])
        return (getattr(model, 'rpl_last_create', None),
            getattr(model, 'rpl_last_update', None),
            getattr(model, 'rpl_last_delete', None))

    @classmethod
    def reset_session(cls):
        cls._session_records = []

    @classmethod
    def _get_mapped_attribute(cls, op):
        mapped_attributes = {
            'created': 'rpl_last_create',
            'modified': 'rpl_last_update',
            'deleted': 'rpl_last_delete'
        }
        return mapped_attributes.get(op)

    @classmethod
    def pull_records(cls, rpc_session):
        repl_data = cls.get_repl_data()

        rpc_model_name = cls._get_remote_name()
        recs = rpc_session.execute('model',
                rpc_model_name,
                'get_to_replicate', repl_data)

        pulled_count = 0
        if recs:
            with Transaction().set_context(**cls._pull_context):
                success, sync_info = cls.synchronise(recs)
                pulled_count = sync_info.get('pulled_count', 0)
                if success:
                    for op, info in recs.items():
                        date_attribute = cls._get_mapped_attribute(op)
                        cls._update_date_model(date_attribute,
                            info['max_date'])
        else:
            logger.info('Model %s: no records to pull' % cls.__name__)

        return pulled_count if cls._notify else 0

    @classmethod
    def to_process_again(cls):
        return cls._to_process_again

    @classmethod
    def _get_local_value(cls, values):
        pool = Pool()
        values = values.copy()

        def get_target_value(Target, unique_id):
            uniq_id_col = Target.unique_id_column
            with Transaction().set_context(active_test=False):
                targets = Target.search([
                        (uniq_id_col, '=', unique_id)])
            if not targets:
                raise InexistantTarget(Target, unique_id)
            target, = targets
            return target

        values_to_remove = set()
        init_values = values.copy()
        for f_name in init_values:
            local_fname = cls._sync_field_mapping.get(f_name, f_name)
            if local_fname not in cls._fields:
                values_to_remove.add(f_name)
                values_to_remove.add(local_fname)
                continue
            field = cls._fields[local_fname]

            if field._type == 'many2one' and values[f_name]:
                Target = field.get_target()
                values[local_fname] = get_target_value(
                    Target, values[f_name]).id

            if field._type == 'reference' and values[f_name]:
                target, unique_id = values[f_name]
                Target = pool.get(target)
                values[local_fname] = str(get_target_value(Target, unique_id))

            if f_name != local_fname:
                if local_fname not in values:
                    values[local_fname] = values[f_name]
                values_to_remove.add(f_name)

        for value_to_remove in values_to_remove:
            del values[value_to_remove]
        return values

    @classmethod
    def _synchronise_one(cls, value):
        if cls._sequenced_field:
            value = cls._clear_sequenced_field(value)
        local_value = cls._get_local_value(value)
        local_key = value[cls.unique_id_column]
        local_records = cls.search([
                (cls.unique_id_column, '=', local_key)])
        if local_records:
            local_record = cls._synchronize_one_record(local_records)
            if not local_record:
                return
            cls.write([local_record], local_value)
            return cls(local_record.id)
        else:
            return cls.create([local_value])[0]

    @classmethod
    def _synchronize_one_record(cls, records):
        return records and records[0] or None

    @classmethod
    def _clear_sequenced_field(cls, value):
        if cls._sequenced_field in value:
            value.pop(cls._sequenced_field)
        return value

    @classmethod
    def _delete_records(cls, wire_ids):
        if wire_ids:
            ctx = cls._pull_context.copy()
            ctx['active_test'] = False
            with Transaction().set_context(**ctx):
                records = cls.search([
                    (cls.unique_id_column, 'in', wire_ids)])
                try:
                    cls.delete(records)
                except backend.DatabaseOperationalError:
                    traceback.print_exc(file=sys.stdout)
                except UserError:
                    traceback.print_exc(file=sys.stdout)
                return len(wire_ids)
        return 0

    @classmethod
    def _compose_per_info(cls, recs_info, del_count):
        '''Compose info about all records persist operation'''
        return {
            'pulled_count': len(recs_info),
            'del_count': del_count}

    @classmethod
    def _compose_per_rec_info(cls, rec):
        '''Compose info about record persist operation'''
        if cls._sequenced_field:
            return {cls._sequenced_field: getattr(rec,
                cls._sequenced_field, '')}
        return {}

    @classmethod
    def synchronise(cls, records):
        cls._to_process_again = []
        success = True

        not_pulled = set(r[cls.unique_id_column] for r in cls._session_records)
        deferred_records = []
        for section, svalues in records.items():
            if section not in ('created', 'modified'):
                continue
            for record in svalues.get('records', []):
                if section == 'created' and cls._sync_deferred_fields:
                    new_record = {}
                    for fname in cls._sync_deferred_fields:
                        if record.get(fname, None):
                            new_record[fname] = record.pop(fname)
                    if new_record:
                        new_record['local_id'] = record['local_id']
                        deferred_records.append(new_record)
                if record not in cls._session_records:
                    cls._session_records.append(record)

        if deferred_records:
            cls._session_records.extend(deferred_records)

        recs = cls._session_records[:]
        recs_info = []
        logger.info('Model %s: record(s) to create or update %s, delete %s' % (
                cls.__name__,
                len(recs) - len(deferred_records),
                len(records.get('deleted', []))))

        del_count = cls._delete_records(records.get('deleted', {}).
            get('records', None))

        for value in recs:
            with Transaction().set_context(active_test=False, modified=False):
                retry = 0
                while retry < 2:
                    try:
                        rec = cls._synchronise_one(value)
                        if rec and value not in deferred_records:
                            recs_info.append(cls._compose_per_rec_info(rec))
                        if value in cls._session_records:
                            cls._session_records.remove(value)
                    except backend.DatabaseOperationalError:
                        traceback.print_exc(file=sys.stdout)
                        retry += 1
                        if retry >= 2:
                            success = False
                        continue
                    except InexistantTarget as target_err:
                        success = False
                        target = target_err.target
                        target.set_required_ids(target_err.id_)
                        if target.__name__ not in cls._to_process_again:
                            cls._to_process_again.append(target.__name__)
                    except UserError as e:
                        success = False
                        traceback.print_exc(file=sys.stdout)
                        if cls._source_replicate_error:
                            raise SourceReplicateError(e.args[-1][0])
                    break

        logger.info('Model %s: record(s) created or updated %s, deleted %s' % (
                cls.__name__, len(recs_info), del_count))

        src_rec_set = set(r[cls.unique_id_column]
            for r in cls._session_records)
        if not_pulled and (not_pulled & src_rec_set) == not_pulled:
            cls._to_process_again = []
        else:
            if cls._to_process_again:
                cls._to_process_again = [cls.__name__] + cls._to_process_again
        return success, cls._compose_per_info(recs_info, del_count)


def _grouper(iterable, n,):
        '''Collect data into fixed-length chunks or blocks'''
        # grouper('ABCDEFG', 3) --> ABC DEF G
        result = []
        tail_index = len(iterable) - (len(iterable) % n)
        if tail_index > 0:
            args = [iter(iterable[:tail_index])] * n
            result.extend(list(zip(*args)))
        result.append(iterable[tail_index:])
        return result


class ReplSrcMixin(ReplMixin):
    """Base class for source replicate models"""

    """Include listed function fields in synchronization"""
    _force_function_fields = []
    """Compute modify date with listed relation fields"""
    _include_modify_date = []
    TAM_SEARCH = 300
    """Define an specific order for search records to replicate"""
    _replication_order = None
    """Define an specific limit and orders for search records to replicate"""
    _replication_limit = None
    _replication_limit_created_order = [('create_date', 'ASC')]
    _replication_limit_modified_order = [('write_date', 'ASC')]

    modify_date = fields.Function(fields.DateTime('Modify date'),
        'get_modify_date', searcher='search_modify_date')

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table__()

        super(ReplSrcMixin, cls).__register__(module_name)

        # Unique id column set
        if not callable(getattr(cls, 'table_query', None)):
            from sql import Column, Null
            cursor = Transaction().connection.cursor()
            unique_id_col = Column(table, cls.unique_id_column)
            cursor.execute(*table.update([unique_id_col], [table.id],
                where=unique_id_col == Null))

    @classmethod
    def __setup__(cls):
        super(ReplSrcMixin, cls).__setup__()
        cls.__rpc__.update({
            'get_to_replicate': RPC(readonly=True),
            'get_fs_info': RPC(readonly=True)})

    @classmethod
    def get_modify_date(cls, records, name):
        res = {}
        for r in records:
            extra_modify_date = [getattr(r, f).modify_date
                for f in cls._include_modify_date]
            modify_date = r.write_date or r.create_date
            res[r.id] = max([d or modify_date for d in extra_modify_date] +
                [modify_date])
        return res

    @classmethod
    def search_modify_date(cls, name, clause):
        domain = ['OR', ]
        prefix = ''
        if '.' in name:
            prefix = '.'.join(name.split('.')[:-1]) + '.'
        for f in [None] + cls._include_modify_date:
            if f is not None:
                Model = Pool().get(getattr(cls, f, ).model_name)
                # use model search method with field name prefix
                fdomain = Model.search_modify_date('%s.%s' % (f, name),
                    clause)
                domain += [fdomain]
            else:
                domain += [
                    [('%swrite_date' % prefix, '=', None),
                        ('%screate_date' % prefix,) + tuple(clause[1:])],
                    [('%swrite_date' % prefix,) + tuple(clause[1:])]]
        return domain

    @classmethod
    def rpc_reset_session(cls, rpc_session):
        rpc_model_name = cls._get_remote_name()
        rpc_session.execute('model', rpc_model_name, 'reset_session')

    @classmethod
    def rpc_to_process_again(cls, rpc_session):
        rpc_model_name = cls._get_remote_name()
        return rpc_session.execute('model', rpc_model_name, 'to_process_again')

    @classmethod
    def _unsent_fields(cls):
        return ('id', 'create_uid', 'write_uid', 'create_date', 'write_date')

    def _get_wire_value(self, field_names=None):
        values = {}

        for f_name, field in list(self._fields.items()):
            if f_name in self._unsent_fields():
                continue
            if field_names and f_name not in field_names:
                continue
            field_type = field._type
            if isinstance(field, fields.Function):
                if f_name not in self._force_function_fields:
                    continue
                field_type = field._field._type
                field = field._field
            elif (isinstance(field, (fields.Many2Many,
                    fields.One2One, fields.One2Many)) and
                    f_name != self.unique_id_column):
                continue
            value = getattr(self, f_name)
            values[f_name] = value
            if field_type == 'many2one':
                target = field.get_target()
                if (issubclass(target, ReplMixin)
                        or hasattr(target, 'unique_id_column')):
                    values[f_name] = (getattr(value, target.unique_id_column)
                        if value else None)
                else:
                    del values[f_name]
            elif field_type == 'reference' and value:
                target = value.__class__
                if issubclass(target, ReplMixin):
                    values[f_name] = (target.__name__,
                        getattr(value, target.unique_id_column))
                else:
                    values[f_name] = None
            elif field_type == 'boolean':
                # convert from sqlite
                values[f_name] = bool(value)

        # Send synch date
        values['synch_date'] = self.modify_date

        return values

    @classmethod
    def _to_repl_modified_dom(cls, repl_data):
        return []

    @classmethod
    def _to_repl_del_dom(cls, repl_data):
        return []

    @classmethod
    def _append_records(cls, values, to_send, section, field_names=None):
        if values:
            if section in ('created', 'modified'):
                values = [r._get_wire_value(field_names) for r in values]
                # '_required_targets' specifies which fields of the model
                # must have a mandatory value to synchronise the record.
                # This is used for models which field
                # of attr 'unique_id_column' is setted manually.
                if hasattr(cls, '_required_targets'):
                    req_targets = getattr(cls, '_required_targets', [])
                    values_ = []
                    for r in values:
                        if all([r.get(target) for target in req_targets]):
                            values_.append(r)
                    values = values_
            to_send.setdefault(section, {})
            to_send[section].setdefault('records', [])
            to_send[section]['records'].extend(values)

    @classmethod
    def get_to_replicate(cls, repl_data):
        to_send = {}
        field_names = repl_data.get('field_names', None)

        required_ids = repl_data.get('required_ids', None)
        # Get inactive records that are refereced by others models
        if required_ids:
            dom = [(cls.unique_id_column, 'in', required_ids)]
            with Transaction().set_context(active_test=False):
                cls._append_records(
                    cls.search(dom, order=cls._replication_order),
                    to_send, 'modified', field_names)
            cls._append_max_date(to_send, update_date=False)
        else:
            cls._append_changes(to_send, repl_data, field_names)
            cls._append_max_date(to_send)

        return to_send

    @classmethod
    def _append_changes(cls, to_send, repl_data, field_names):
        l_create = repl_data.get('l_create', None)
        l_delete = repl_data.get('l_delete', None)
        l_update = repl_data.get('l_update', None)
        with Transaction().set_context(active_test=False):
            if l_update or l_create:
                # only write/delete if synched once
                to_delete = cls._later_delete_synch_date(l_delete)
                cls._append_records(to_delete, to_send, 'deleted')
                dom = cls._to_repl_write_dom(l_update or l_create)
                cls._append_records(
                    cls.search(dom,
                        order=(cls._replication_limit
                            and cls._replication_limit_modified_order
                            or cls._replication_order),
                        limit=cls._replication_limit),
                    to_send, 'modified')
            dom = cls._to_repl_create_dom(l_create)
            cls._append_records(
                cls.search(dom,
                    order=(cls._replication_limit
                        and cls._replication_limit_created_order
                        or cls._replication_order),
                    limit=cls._replication_limit),
                to_send, 'created', field_names)

    def _to_repl_dom(fieldname):
        @classmethod
        def to_repl_field_dom(cls, date):
            if not date:
                return []
            base_domain = [(fieldname, '>', date)]
            if not cls._include_modify_date:
                return base_domain

            base_domain, = base_domain
            domain = ['OR', base_domain]
            for f in cls._include_modify_date:
                domain += [('%s.%s' % (f, base_domain[0]),
                    ) + tuple(base_domain[1:])]
            return domain
        return to_repl_field_dom

    _to_repl_write_dom = _to_repl_dom('modify_date')
    _to_repl_create_dom = _to_repl_dom('create_date')

    @classmethod
    def _append_max_date(cls, to_send, update_date=True):
        if update_date:
            for op, info in to_send.items():
                dates = [record['synch_date']
                    for record in info['records'] if record['synch_date']]
                max_date = dates and max(dates) or None
                info['max_date'] = max_date
                for record in info['records']:
                    record.pop('synch_date', None)
                if op == 'deleted':
                    info['records'] = [d['id'] for d in info['records']]
        else:
            for op, info in to_send.items():
                info['max_date'] = None

    @classmethod
    def _process_per_info(cls, per_info, recs):
        '''Process  persistence information'''
        pushed_count = per_info.get('pulled_count', 0)
        logger.info('Model %s: record(s) created or updated %s, deleted %s' % (
                cls.__name__, pushed_count, per_info.get('del_count', 0)))
        return pushed_count if cls._notify else 0

    @classmethod
    def push_records(cls, rpc_session, update_modified=True):
        rpc_model_name = cls._get_remote_name()
        repl_data = rpc_session.execute('model', rpc_model_name,
            'get_repl_data')

        recs = cls.get_to_replicate(repl_data)
        if recs:
            logger.info(('Model %s: record(s) to create or update %s, '
                'delete %s') % (
                    cls.__name__,
                    (len(recs.get('created', []))
                        + len(recs.get('modified', []))),
                    len(recs.get('deleted', []))))
            per_info = rpc_session.execute('model', rpc_model_name,
                    'synchronise', recs, context=cls._pull_context)
            success, per_info = per_info

            if 'modified' in recs:
                # source side send all created/writed records as modified.
                # destination side must tell us the right operation per record.
                recs['created'] = recs['modified'].copy()
            for op, info in recs.items():
                date_attribute = cls._get_mapped_attribute(op)
                max_date = info['max_date']
                values = [r for r in per_info.get('recs_info', [])
                    if r.get('operation', op) == op]
                if values:
                    max_date = max(v.get('synch_date', info['max_date'])
                        for v in values)
                elif op in ('created', 'modified') and \
                        per_info.get('recs_info', []):
                    # there are changes but not for this operation
                    max_date = None
                if max_date:
                    cls._update_date_model(date_attribute, max_date)
                cls._deactivate_modified(recs, update_modified=update_modified)
            return cls._process_per_info(per_info, recs)
        else:
            logger.info('Model %s: no records to push' % cls.__name__)
            return 0

    @classmethod
    def _deactivate_modified(cls, records, update_modified=True):
        pass

    @classmethod
    def _save_repl_deleted(cls, records):
        ReplDeleted = Pool().get('replication.deleted')

        deleted_list = []
        for record in records:
            rd = ReplDeleted(record_id=record.id, model_name=record.__name__)
            setattr(rd, cls.unique_id_column, getattr(
                record, cls.unique_id_column, None))
            deleted_list.append(rd)
        ReplDeleted.save(deleted_list)

    @classmethod
    def delete(cls, records):
        if not Transaction().context.get('synch', False):
            cls._save_repl_deleted(records)
        super(ReplSrcMixin, cls).delete(records)

    @classmethod
    def _later_delete_synch_date(cls, modif_date):
        ReplDeleted = Pool().get('replication.deleted')

        results = ReplDeleted.search([
            ('create_date', '>', modif_date) if modif_date else (),
            ('model_name', '=', cls.__name__),
            ['OR',
                ('user', '=', Transaction().user),
                ('user', '=', None)]
            ], limit=100)
        return [{'id': getattr(result, cls.unique_id_column, None),
            'synch_date': result.create_date} for result in results]


class ReplLocalIdMixin(object):
    """Base class for replicate models via local ID"""

    unique_id_column = 'local_id'

    local_id = fields.Integer('Local ID', readonly=True)


class ReplLocalIdDestMixin(ReplLocalIdMixin, ReplDestMixin):
    """Base class for destination replicate models via local ID"""

    @classmethod
    def __setup__(cls):
        super(ReplLocalIdMixin, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('local_id_uk', Unique(t, t.local_id),
                'Local ID must be unique.')]


class ReplLocalIdSrcMixin(ReplLocalIdMixin, ReplSrcMixin):
    """Base class for source replicate models via local ID"""

    @classmethod
    def __setup__(cls):
        super(ReplLocalIdMixin, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('local_id_uk', Unique(t, t.local_id),
                'Local ID must be unique.')]

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['local_id'] = None
        return super(ReplLocalIdSrcMixin, cls).copy(records, default=default)

    @classmethod
    def create(cls, vlist):
        records = super(ReplLocalIdSrcMixin, cls).create(vlist)
        changes = []
        for r in records:
            if not r.local_id:
                changes.extend([[r], {'local_id': r.id}])
        if changes:
            cls.write(*changes)
        records = cls.browse(list(map(int, records)))
        return records


class ReplUUIdMixin(object):
    """Base class for replicate models via UUID"""

    unique_id_column = 'uuid'
    uuid = fields.Char('Universal Unique Identifier', required=True,
        readonly=True, select=True)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['uuid'] = None
        return super(ReplUUIdMixin, cls).copy(records, default=default)


class ReplUUIdDestMixin(ReplUUIdMixin, ReplDestMixin):
    """Base class for detination replicate models via UUID"""

    @classmethod
    def __setup__(cls):
        super(ReplUUIdDestMixin, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [('uuid_uk', Unique(t, t.uuid),
            'UUID must be unique.')]


class ReplUUIdSrcMixin(ReplUUIdMixin, ReplSrcMixin):
    """Base class for source replicate models via UUID"""

    @classmethod
    def __setup__(cls):
        super(ReplUUIdSrcMixin, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [('uuid_uk', Unique(t, t.uuid),
            'UUID must be unique.')]

    @classmethod
    def create(cls, vlist):
        for vals in vlist:
            if not vals.get('uuid'):
                vals['uuid'] = uuid.uuid1().hex
        return super(ReplUUIdSrcMixin, cls).create(vlist)


class ReplDuplexMixin(ReplSrcMixin, ReplDestMixin):
    """Base class for duplex replicate models"""

    """Define states in which a workflow and its details can be synchronized"""
    """It is a tuple with state field name and the list of valid states"""
    _valid_sync_workflow_states = None


class ReplMasterDuplexMixin(ReplDuplexMixin):
    """Base class for master duplex replicate models"""
    _source_replicate_error = True

    # dest
    @classmethod
    def _compose_per_info(cls, recs_info, del_count):
        '''Compose info about all records persist operation'''
        info = super(ReplMasterDuplexMixin,
            cls)._compose_per_info(recs_info, del_count)
        info['recs_info'] = recs_info
        return info

    @classmethod
    def _compose_per_rec_info(cls, rec):
        '''Compose info about record persist operation'''
        info = super(ReplMasterDuplexMixin, cls)._compose_per_rec_info(rec)
        info['unique_id_column'] = getattr(rec, cls.unique_id_column)
        info['synch_date'] = rec.modify_date
        info['operation'] = (not rec.write_date and 'created') or (
            rec.write_date and 'modified') or 'deleted'
        return info

    @classmethod
    def _synchronize_one_record(cls, records):
        record = super()._synchronize_one_record(records)
        if record and (isinstance(cls, Workflow) or
                cls._valid_sync_workflow_states):
            state_field, states = cls._valid_sync_workflow_states
            if getattr(record, state_field) not in states:
                return None
        return record


class ReplSlavSrcMixin(ReplSrcMixin):

    modified = fields.Boolean('Modified')

    @staticmethod
    def default_modified():
        return False

    @classmethod
    def create(cls, vlist):
        records = super(ReplSlavSrcMixin, cls).create(vlist)
        if Transaction().context.get('modified', True):
            cls.write(records, {'modified': True})
        return records

    @classmethod
    def _append_changes(cls, to_send, repl_data, field_names):
        l_delete = repl_data.get('l_delete', None)
        with Transaction().set_context(active_test=False):
            to_delete = cls._later_delete_synch_date(l_delete)
            cls._append_records(to_delete, to_send, 'deleted')

            def _domain(operator):
                dom = [
                    ('modified', '=', True),
                    ('write_date', operator, None)
                ]
                if isinstance(cls, Workflow) or \
                        cls._valid_sync_workflow_states:
                    state_field, states = cls._valid_sync_workflow_states
                    dom.append(
                        (state_field, 'in', states))
                return dom

            cls._append_records(cls.search(_domain('=')), to_send, 'created')
            cls._append_records(cls.search(_domain('!=')), to_send, 'modified')

    @classmethod
    def _deactivate_modified(cls, records, update_modified=True):
        ReplDeleted = Pool().get('replication.deleted')

        for op, info in records.items():
            if op == 'deleted':
                to_delete = ReplDeleted.search([
                    ('uuid', 'in', info['records'])])
                ReplDeleted.delete(to_delete)
            elif update_modified:
                uuids = [r['uuid'] for r in info['records']]
                modified_records = cls.search([('uuid', 'in', uuids)])
                ctx = cls._pull_context.copy()
                ctx['modified'] = False
                with Transaction().set_context(**ctx):
                    cls.write(modified_records, {'modified': False})

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        args = []
        for records, values in zip(actions, actions):
            if Transaction().context.get('modified', True):
                values['modified'] = True
            args.extend((records, values))
        super(ReplSlavSrcMixin, cls).write(*args)

    @classmethod
    def _unsent_fields(cls):
        return super(ReplSlavSrcMixin, cls)._unsent_fields() + ('modified',)


class ReplSlavDuplexMixin(ReplDuplexMixin, ReplSlavSrcMixin):
    """Base class for slave duplex replicate models"""
    _sync_priority = 20

    @classmethod
    def _clear_sequenced_field(cls, value):
        return value

    @classmethod
    def _compose_per_rec_info(cls, rec):
        info = super(ReplSlavDuplexMixin, cls)._compose_per_rec_info(rec)
        info.pop(cls._sequenced_field, None)
        return info

    @classmethod
    def _process_per_info(cls, per_info, recs):
        '''Process  persistence information'''

        for rec_info in per_info.get('recs_info', []):
            r = cls.search([
                (cls.unique_id_column, '=', rec_info['unique_id_column'])])
            if cls._sequenced_field:
                ctx = cls._pull_context.copy()
                ctx['modified'] = False
                with Transaction().set_context(**ctx):
                    cls.write(r, {
                        cls._sequenced_field: rec_info[cls._sequenced_field]})

        return super(ReplSlavDuplexMixin,
            cls)._process_per_info(per_info, recs)


class ReplUUIdMasterDuplexMixin(ReplUUIdMixin, ReplMasterDuplexMixin):
    """Base class for master duplex replicate models via UUID"""

    @classmethod
    def __setup__(cls):
        super(ReplUUIdMasterDuplexMixin, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [('uuid_uk', Unique(t, t.uuid),
            'UUID must be unique.')]

    @classmethod
    def create(cls, vlist):
        for vals in vlist:
            if not vals.get('uuid'):
                vals['uuid'] = uuid.uuid1().hex
        return super(ReplUUIdMasterDuplexMixin, cls).create(vlist)


class ReplUUIdSlavDuplexMixin(ReplUUIdMixin, ReplSlavDuplexMixin):
    """Base class for duplex replicate models via UUID"""

    @classmethod
    def __setup__(cls):
        super(ReplUUIdSlavDuplexMixin, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [('uuid_uk', Unique(t, t.uuid),
            'UUID must be unique.')]

    @classmethod
    def create(cls, vlist):
        for vals in vlist:
            if not vals.get('uuid'):
                vals['uuid'] = uuid.uuid1().hex
        return super(ReplUUIdSlavDuplexMixin, cls).create(vlist)


class ReplDeleted(ModelView, ModelSQL):
    '''Class to store the deleted records'''
    __name__ = 'replication.deleted'

    model_name = fields.Char('Model Name')
    record_id = fields.Integer('Record Id')
    synch_date = fields.Timestamp('Synchronisation date',
        readonly=True, select=True)
    local_id = fields.Integer('Local ID', readonly=True)
    uuid = fields.Char('Universal Unique Identifier',
        readonly=True, select=True)
    user = fields.Many2One('res.user', 'User')

    @classmethod
    def clear_deleted_entries(cls, days_ago):
        to_del = cls.search([
            ('create_date', '<=',
                datetime.today() - relativedelta(days=days_ago))
            ])
        if to_del:
            cls.delete(to_del)


class Model(metaclass=PoolMeta):
    __name__ = 'ir.model'

    rpl_last_create = fields.Timestamp('Replication last create', select=True)
    rpl_last_delete = fields.Timestamp('Replication last delete', select=True)
    rpl_last_update = fields.Timestamp('Replication last update', select=True)


class Warning_(metaclass=PoolMeta):
    __name__ = 'res.user.warning'

    @classmethod
    def check(cls, warning_name):
        if Transaction().context.get('synch', False):
            return False
        return super(Warning_, cls).check(warning_name)


class ReplicationLog(ModelView, ModelSQL):
    '''Class to store the replication error'''
    __name__ = 'replication.log'

    exception_type = fields.Char('Exception type', required=True)
    trace = fields.Text('Trace', required=True)
    revised = fields.Boolean('Revised')

    @classmethod
    def __setup__(cls):
        super(ReplicationLog, cls).__setup__()
        cls.__rpc__.update({
            'register_log': RPC(readonly=False)})

    @staticmethod
    def default_revised():
        return False

    @classmethod
    def is_to_register(cls, trace):
        r_logs = cls.search([
            ('revised', '=', True),
            ('create_uid', '=', Transaction().user),
            ('trace', '=', trace)
        ])
        return not r_logs

    @classmethod
    def register_log(cls, exception_type, trace):
        if cls.is_to_register(trace):
            replicaton_log = ReplicationLog(exception_type=exception_type,
                trace=trace)
            replicaton_log.save()


class UserLocalIdSrc(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'res.user'
    _sync_fields = {'language', 'local_id'}

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.__rpc__.update({
            'attach_db_to_user': RPC(readonly=False)})

    @classmethod
    def attach_db_to_user(cls, filename, data):
        pool = Pool()
        Attachment = pool.get('ir.attachment')
        with Transaction().set_context(_check_access=False):
            user, = cls.search([(
                    'id', '=', Transaction().user)
                ])
            user.upload_replica_db = False
            user.save()
            Transaction().commit()
            attachment = Attachment(
                name=str(datetime.now()) + '-' + filename + '.gz',
                type='data',
                data=data,
                resource=user)
            attachment.save()

    @classmethod
    def _unsent_fields(cls):
        return set(cls._fields.keys()) - cls._sync_fields

    @classmethod
    def get_to_replicate(cls, repl_data):
        repl_data['required_ids'] = [Transaction().user]
        return super().get_to_replicate(repl_data)


class UserLocalIdDest(ReplLocalIdDestMixin, metaclass=PoolMeta):
    __name__ = 'res.user'


class UserFilterModelMixin(ReplLocalIdSrcMixin):
    """
    This Mixin class allows to filter its data depending on user.
    The records are synchronized taking into account a relation of records
    with the replication user.
    """
    """Name of o2m field where records of current model are stored per user."""
    _sync_user_data_field = ''

    @classmethod
    def _get_sync_user_model_field(cls):
        field = getattr(cls, cls._sync_user_data_field)
        if isinstance(field, fields.Function):
            field = field._field
        return field.model_name, field.field

    @classmethod
    def get_modify_date(cls, records, names):
        pool = Pool()
        model, field = cls._get_sync_user_model_field()
        UserModel = pool.get(model)

        # adapt modify date to take user changes into account
        user_records = UserModel.search([
            ('user', '=', Transaction().user),
            (field, 'in', [r.id for r in records])])
        user_records = {getattr(u, field).id: u.write_date or u.create_date
            for u in user_records}
        res = {}
        for r in records:
            user_record_date = r.write_date or r.create_date
            res[r.id] = max(user_records.get(r.id, None) or user_record_date,
                user_record_date)
        return {'modify_date': res}

    @classmethod
    def search_modify_date(cls, name, clause):
        pool = Pool()
        model, field = cls._get_sync_user_model_field()
        UserModel = pool.get(model)

        domain = super().search_modify_date(name, clause)
        user_domain = UserModel.search_modify_date(name, clause)
        prefix = ''
        if '.' in name:
            prefix = '.'.join(name.split('.')[:-1]) + '.'
        return domain + [[
            ('%s%s' % (prefix, cls._sync_user_data_field),
                'where', user_domain)],
        ]

    @classmethod
    def _get_date_domain(cls, date, field_name):
        pool = Pool()
        model, field = cls._get_sync_user_model_field()
        UserModel = pool.get(model)

        user_records = UserModel.search([
            ('user', '=', Transaction().user)])
        record_ids = []
        for record in user_records:
            record_date = getattr(record, field_name, None)
            if not record_date or (date and record_date <= date):
                continue
            record_ids.append(getattr(record, field).id)
        return record_ids

    @classmethod
    def _to_repl_create_dom(cls, date):
        res = super()._to_repl_create_dom(date)

        record_ids = cls._get_date_domain(date, 'create_date')
        if not record_ids:
            return res
        return ['OR', ('id', 'in', record_ids), res]

    @classmethod
    def _to_repl_write_dom(cls, date):
        res = super()._to_repl_write_dom(date)

        record_ids = cls._get_date_domain(date, 'write_date')
        if not record_ids:
            return res
        return ['OR', ('id', 'in', record_ids), res]


class UserFilterModelRelationMixin(object):
    _sync_data_field = ''

    @classmethod
    def _save_repl_deleted(cls, records):
        ReplDeleted = Pool().get('replication.deleted')

        deleted_list = []
        for record in records:
            model_record = getattr(record, cls._sync_data_field)
            rd = ReplDeleted(
                record_id=model_record.id,
                user=record.user,
                model_name=model_record.__name__)
            setattr(rd, model_record.unique_id_column, getattr(
                model_record, model_record.unique_id_column, None))
            deleted_list.append(rd)
        ReplDeleted.save(deleted_list)

    @classmethod
    def delete(cls, records):
        if not Transaction().context.get('synch', False):
            cls._save_repl_deleted(records)
        super().delete(records)

    @classmethod
    def create(cls, vlist):
        ReplDeleted = Pool().get('replication.deleted')

        records = super().create(vlist)

        sorted_records = sorted(records, key=lambda r: r.user.id)
        repl_deleted = []
        for user_id, grouped_records in groupby(sorted_records,
                key=lambda r: r.user.id):
            grouped_records = [getattr(record, cls._sync_data_field)
                for record in grouped_records]
            repl_deleted.extend(ReplDeleted.search([
                ('user', '=', user_id),
                ('model_name', '=', grouped_records[0].__name__),
                ('record_id', 'in', list(map(int, grouped_records)))
            ]))
        if repl_deleted:
            ReplDeleted.delete(repl_deleted)
        return records

    @classmethod
    def search_modify_date(cls, name, clause):
        return [
            ('user', '=', Transaction().user),
            ['OR', [
                    ('write_date', '=', None),
                    ('create_date',) + tuple(clause[1:])
                ],
                [
                    ('write_date',) + tuple(clause[1:])]
            ]
        ]
